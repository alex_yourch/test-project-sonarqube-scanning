<?php declare(strict_types = 1);

class HelloWorld
{
    public function sayHello(DateTimeImmutable $date): void
    {
        echo 'Hello, ' . $date->format('j. n. Y');
    }

    public function sayHelloCustom11111111111111111(DateTimeImmutable $date): void
    {
        echo 'Custom, ' . $date->format('j. n. Y');
    }
}